export class DataService {

  private _filters = {
    specialists: []
  };
  private _messages = {
    'С 007': "На выбранный период отсутствуют свободные временные интервалы для записи. Выберите другой период.",
    'С 008': "Для просмотра расписания выберите хотя бы один Доступный ресурс.",
    'С 009': "Запись создана"
  };

  get filters() {
    return this._filters;
  };

  // @ngInject
  constructor(/*private $http: ng.IHttpService*/) {
  }

  getMessage = (code: string): string => {
    return this._messages[code] || '';
  };

  setFilterProp = (name: string, value) => {
    this._filters[name] = value;
  };

}
