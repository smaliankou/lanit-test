import { EndpointService } from "./EndpointService";
import IPromise = angular.IPromise;


export class MedicalInstitutionsService {

  private _MI = [
    {
      id: 1,
      name: "ГП №128",
      startTime: {
        h: "08",
        m: "00"
      },
      endTime: {
        h: "21",
        m: "00"
      }
    }
  ];

  public model: MedicalInstitution[] = [];

  // @ngInject
  constructor(private $q: ng.IQService) {
  }

  public getAll(): IPromise<any> {
    return this.getMI().then((data: any) => this.handleResponse(data));
  }

  private getMI() {
    let defer = this.$q.defer();
    setTimeout(() => defer.resolve({data: this._MI}));
    return defer.promise;
  }

  private handleResponse(response: any): any {
    this.setModel(response.data);
    return response;
  }

  private setModel(data: MedicalInstitution[]): void {
    this.model.length = 0;
    Array.prototype.push.apply(this.model, data);
  }

}

export interface MedicalInstitution {
  id: number,
  name: string,
  startWork: number
  endWork: number
}
