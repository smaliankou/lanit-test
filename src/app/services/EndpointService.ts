export class EndpointService {
  public getUrl(moduleName: string): string {
    return moduleName + ".json";
  }
}
