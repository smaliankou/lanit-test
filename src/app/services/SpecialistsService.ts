import { EndpointService } from "./EndpointService";
import IPromise = angular.IPromise;

//TODO-generic refactor with generics
export class SpecialistsService {

  public model: Specialist[] = [];
  private moduleName: string = "specialists";

  // @ngInject
  constructor(private endpointService: EndpointService,
              private $http: ng.IHttpService) {
  }

  public getAll(): IPromise<any> {
    return this.$http
      .get(this.endpointService.getUrl(this.moduleName))
      .then((data: any) => this.handleResponse(data));
  }

  private handleResponse(response: any): any {
    this.setModel(response.data);
    return response;
  }

  private setModel(data: Specialist[]): void {
    this.model.length = 0;
    Array.prototype.push.apply(this.model, data);
  }

  /**
   * Generate demo specialist schedule
   * @param id - Specialist Id
   */
  private generateSchedule(id: number) {
    let today = new Date(),
      TYPE_WORK_OFF = 0,
      TYPE_WORK = 1,
      TYPE_WORK_STUDY = 2;
    if (id === 1) {
      let startTime: 10, endTime: 20;
      let work = [{
        startTime: {h: 10, m: 0},
        endTime: {h: 20, m: 0},
        step: 30,
        days: [1, 2, 3, 4, 5],
        daysRange: 60,
      }];
      // let study = {
      //   startTime: 10, endTime: 20, step: 30, days: [1, 2, 3, 4, 5],
      // };
      let item = {
        place: {
          mi: 1,
          room: "к.110"
        }
      };

    }
  }
}

export interface Specialist {
  id: number,
  name: string,
  specialty: string,
  schedule?: Object[]
}
