import { EndpointService } from "./EndpointService";
import IPromise = angular.IPromise;

export class PatientsService {

  public model: IPatient[] = [];
  private moduleName: string = "patients";

  // @ngInject
  constructor(private endpointService: EndpointService,
              private $http: ng.IHttpService) {
  }

  public getAll(): IPromise<any> {
    return this.$http
      .get(this.endpointService.getUrl(this.moduleName))
      .then((data: any) => this.handleResponse(data));
  }

  private handleResponse(response: any): any {
    this.setModel(response.data);
    return response;
  }

  private setModel(data: IPatient[]): void {
    this.model.length = 0;
    Array.prototype.push.apply(this.model, data);
  }

}

export interface IPatient {
  id: number;
  name: string;
  dateOfBirth: string;
  OMS: number;
}
