export function groupBy() {
    return function(collection: Object[], key: string)
    {

            if (collection === null) return;
            return uniqueItems(collection, key);

        /*forEach( collection, function( elm ) {
            console.log(elm);
            prop = elm[key];

            if(!result[prop]) {
                result[prop] = [];
            }
            result[prop].push(elm);
        });
        console.log(result);
        return result;
        console.log(collection, key);
        console.log(collection.reduce( (r, a) => {
            r[a[key]] = r[a[key]] || [];
            r[a[key]].push(a);
            return r;
        }, {}));
        return collection.reduce( (r, a) => {
            r[a[key]] = r[a[key]] || [];
            r[a[key]].push(a);
            return r;
        }, {})*/
    }

    /*return (value)=> {
     return $sce.trustAsResourceUrl(value)
     };*/
}

var uniqueItems = function (data, key) {
    var result = [];
    for (var i = 0; i < data.length; i++) {
        var value = data[i][key];
        if (result.indexOf(value) == -1) {
            result.push(value);
        }
    }
    return result;
};