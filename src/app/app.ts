import * as angular from "angular";
import { IState, IStateProvider, IUrlRouterProvider } from "angular-ui-router";

import { DashboardCtrl } from "./controllers/DashboardCtrl";

import { DataService } from "./services/DataService";
import { EndpointService } from "./services/EndpointService";
import { MedicalInstitutionsService } from "./services/MedicalInstitutionsService";
import { PatientsService } from "./services/PatientsService";
import { SpecialistsService } from "./services/SpecialistsService";

import { DateAppointmentDirective } from "./directives/date/DateAppointmentDirective";
import { PatientsDirective } from "./directives/patients/PatientsDirective";
import { ScheduleDirective, SpecialistDirective } from "./directives/schedule/ScheduleDirective";
import { SpecialistsDirective } from "./directives/specialists/SpecialistsDirective";

import { groupBy } from "./filters/GroupByFilter";

let app = angular.module("app", ["ui.router", 'ui.bootstrap', 'perfect_scrollbar']);
app.controller("DashboardCtrl", DashboardCtrl);

app.config(($stateProvider: IStateProvider, $urlRouterProvider: IUrlRouterProvider) => {
  $stateProvider
    .state('index', <IState>{
      url: "/",
      templateUrl: "templates/app.html",
      controller: DashboardCtrl,
      controllerAs: "DBC"
    });

  $urlRouterProvider.otherwise('/');
});

// app.controller('DashboardCtrl', DashboardCtrl);
app.service("dataService", DataService);
app.service("patientsService", PatientsService);
app.service("specialistsService", SpecialistsService);
app.service("medicalInstitutionsService", MedicalInstitutionsService);
app.service("endpointService", EndpointService);
app.directive("dateAppointment", <any>DateAppointmentDirective);
app.directive("patientsBox", <any>PatientsDirective);
app.directive("specialistsBox", <any>SpecialistsDirective);
app.directive("schedule", <any>ScheduleDirective);
app.directive("specialist", <any>SpecialistDirective);
app.filter("groupBy", groupBy);

const App: ng.IAngularStatic = angular;

export { App };
