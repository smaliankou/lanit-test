import * as angular from 'angular';
import { directive } from "../../decorators/directive";
import { IPatient, PatientsService } from "../../services/PatientsService";
import { DataService } from "../../services/DataService";

@directive('dataService', 'patientsService')
export class PatientsDirective implements ng.IDirective {

  public templateUrl: string = 'directives/patients/templates/patients.directive.html';
  public restrict: string = "EA";
  public replace: boolean = true;
  public scope = {};

  public patients: IPatient[];

  public link: ng.IDirectiveLinkFn = (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes): void => {
    scope.patients = this.patients;

    scope.startsWith = (search: string) => {
      return (patient: IPatient) => {
        search = angular.lowercase(search);
        return angular.lowercase(patient.name).lastIndexOf(search, 0) === 0 || patient.OMS.toString().lastIndexOf(search, 0) === 0;
        // return angular.isNumber(+search[0]) ? patient.OMS.toString().lastIndexOf(search, 0) === 0 : angular.lowercase(patient.name).lastIndexOf(search, 0) === 0;
      }
    };

    scope.selected = ($item, $model, $label) => {
      scope.selectedPatient = $model;
    };

    scope.$watch('selectedPatient', (nV) => {
      this.dataService.setFilterProp('patient', nV);
      scope.$root.$broadcast('schedule.rebuild');
    });

  };

  // @ngInject
  constructor(private dataService: DataService,
              private patientsService: PatientsService) {

    this.patients = patientsService.model;
    patientsService.getAll();
  }

}
