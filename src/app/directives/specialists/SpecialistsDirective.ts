import * as angular from 'angular';
import { directive } from "../../decorators/directive";
import { DataService } from "../../services/DataService";
import { Specialist, SpecialistsService } from "../../services/SpecialistsService";

@directive('dataService', 'specialistsService')
export class SpecialistsDirective implements ng.IDirective {

  public templateUrl: string = 'directives/specialists/templates/specialists.directive.html';
  public restrict: string = "EA";
  public replace: boolean = true;
  public scope = {};

  public specialists: Specialist[];

  public link: ng.IDirectiveLinkFn = (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes): void => {
    const FILTER_MODE_SPECIALTIES = 'Specialties';
    const FILTER_MODE_ALPHABETICALLY = 'Alphabetically';

    scope.sortMode = FILTER_MODE_SPECIALTIES;
    scope.selectedSpecialists = 0;
    scope.specialistGroup = {};
    scope.specialistGroupSelectedReload = {};

    scope.specialists = this.specialists;

    scope.specialistGroupSelected = (specialty, reload) => {
      if (reload === true) {
        scope.specialists.forEach((s) => {
          if (s.specialty === specialty) {
            s.selected = scope.specialistGroup[specialty];
          }
        });
        scope.specialistGroupSelectedReload[specialty] = false;
      }
    };

    scope.specialistSelected = (specialist) => {
      scope.specialistGroup[specialist.specialty] = scope.specialists.filter((s) => s.specialty === specialist.specialty).every((s) => s.selected);
    };

    scope.selectAll = (state: boolean) => {
      scope.specialists.forEach((s) => {
        s.selected = state;
        scope.specialistSelected(s);
      });
    };

    scope.startsWith = (search: string) => {
      return (specialist: Specialist) => {
        search = angular.lowercase(search);
        return angular.lowercase(specialist.name).lastIndexOf(search, 0) === 0 || specialist.specialty.lastIndexOf(search, 0) === 0;
      }
    };

    scope.selected = ($item, $model, $label) => {
      $model.selected = true;
      scope.specialists.forEach((s) => {
        scope.specialistSelected(s);
      });
    };

    scope.$watch('sortMode', (nV) => {
      if (nV === FILTER_MODE_SPECIALTIES) {
        scope.specialists.forEach((s) => {
          scope.specialistSelected(s);
        });
      }
    });

    scope.$watchCollection(() => {
      return scope.specialists.filter((s) => !!s.selected);
    }, (nV, Ov) => {
      // console.log('change selectedSpecialists');
      scope.selectedSpecialists = nV['length'] || 0;
      this.dataService.setFilterProp('specialists', nV);
      scope.$root.$broadcast('schedule.rebuild');
    });
  };

  // @ngInject
  constructor(private dataService: DataService,
              private specialistsService: SpecialistsService) {

    this.specialists = specialistsService.model;
    specialistsService.getAll();
  }

}
