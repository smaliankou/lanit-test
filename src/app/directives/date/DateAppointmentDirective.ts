import { directive } from "../../decorators/directive";
import { DataService } from "../../services/DataService";

@directive('dataService')
export class DateAppointmentDirective implements ng.IDirective {

  public templateUrl: string = 'directives/date/templates/date-appointment.directive.html';
  public restrict: string = "EA";
  public replace: boolean = true;
  public scope = {};

  public link: ng.IDirectiveLinkFn = (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes): void => {

    scope.today = new Date();
    scope.dtFormat = 'dd.MM.yyyy';
    scope.toggle = () => {
      scope.datePopup.opened = !scope.datePopup.opened;
    };
    scope.datePopup = {
      opened: false
    };
    scope.dateOptions = {
      dateDisabled: function (data) {
        let date = data.date,
          mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
      },
      maxDate: new Date().setDate(new Date().getDate() + 14),
      minDate: new Date(),
      startingDay: 1
    };

    scope.disabled = true;

    scope.$watch('disabled', (nV) => {
      scope.currentDate = nV ? undefined : new Date();
    });

    scope.$watch('currentDate', (nV) => {
      this.dataService.setFilterProp('currentDate', nV);
      scope.$root.$broadcast('schedule.rebuild');
    });

    scope.$root.$on('schedule.rebuild', () => {
      scope.filters = this.dataService.filters;
      scope.disabled = !(scope.filters['specialists'] && scope.filters['specialists']['length'] > 0);
    });
  };

  // @ngInject
  constructor(private dataService: DataService) {
  }

}
