import * as angular from 'angular';
import { directive } from "../../decorators/directive";
import { DataService } from "../../services/DataService";
import { MedicalInstitution, MedicalInstitutionsService } from "../../services/MedicalInstitutionsService";

@directive('dataService', 'medicalInstitutionsService', '$filter', '$modal')
export class ScheduleDirective implements ng.IDirective {

  public templateUrl: string = 'directives/schedule/templates/schedule.directive.html';
  public restrict: string = "EA";
  public replace: boolean = true;

  private WORK_TYPES = {};
  private medicalInstitutions: MedicalInstitution[];

  public link: ng.IDirectiveLinkFn = (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes): void => {

    let el = element;

    scope.emptyMessage;
    scope.today = new Date();
    scope.medicalInstitutions = this.medicalInstitutions;

    scope.getTypeOfWork = (type) => {
      return this.WORK_TYPES[type];
    };

    scope.filters = this.dataService.filters;
    scope.specialists = this.prepareByPeriod(scope.filters['specialists'], scope.filters['viewPeriod'], scope.filters['currentDate']);

    scope.emptyMessage = this.getMessage(scope.specialists);

    scope.$root.$on('schedule.rebuild', () => {
      // console.log('schedule.rebuild');
      scope.filters = this.dataService.filters;
      scope.specialists = this.prepareByPeriod(scope.filters['specialists'], scope.filters['viewPeriod'], scope.filters['currentDate']);
      scope.emptyMessage = this.getMessage(scope.specialists);
    });

    scope.onScheduleScroll = (scrollTop, scrollHeight, scrollLeft, scrollWidth) => {
      let head = el.find('.schedule-scroller-head');
      scope.$root.$broadcast('schedule.scroll', {scroll: scrollTop});
      head['scrollLeft'](scrollLeft);
    };

    scope.setCellData = (cell, specialist) => {
      scope.currentCell = cell;
      scope.currentSpecialist = specialist;
      scope.disabledByTime = (new Date(cell.startTime)).getTime() <= (new Date()).getTime();

      if (this.dataService.filters['patient']) {
        scope.selectedPatient = this.dataService.filters['patient'];
        scope.cellSelectedPatient = undefined;
        if (cell['patientRecords']) {
          // let cr = cell['patientRecords'].find((cr) => cr.patient.id === this.dataService.filters['patient'].id);
          // scope.cellSelectedPatient = cr ? cr['patient'] : cr;
          scope.cellSelectedPatient = cell['patientRecords'].find((cr) => cr.patient.id === this.dataService.filters['patient'].id);
        }
      }

    };

    scope.viewRecord = () => {
      // console.log('viewRecord');

      let modalInstance = this.$modal.open({
        animation: scope.animationsEnabled,
        templateUrl: 'cellViewRecordModal.html',
        windowClass: 'schedule-cell-view-record-modal',
        controller: ['$scope', '$modalInstance', 'data', ($scope, $modalInstance, data) => {
          $scope.data = data;

          $scope.cancel = () => {
            $modalInstance.dismiss('cancel');
          };
        }],
        size: 'sm',
        resolve: {
          data: () => {
            return {
              cell: scope.currentCell,
              cellSelectedPatient: scope.cellSelectedPatient,
              specialist: scope.currentSpecialist
            };
          }
        }
      });

      /*modalInstance.result.then((selectedItem) => {});*/

    };

    scope.addRecord = () => {
      // console.log('addRecord');

      scope.currentSpecialist.patients.push({
        "date": {
          "date": this.$filter('date')(scope.currentCell.startTime, "dd.MM.yyyy"),
          "time": {
            "h": this.$filter('date')(scope.currentCell.startTime, "HH"),
            "m": this.$filter('date')(scope.currentCell.startTime, "mm")
          }
        },
        "patient": scope.selectedPatient
      });

      scope.$root.$broadcast('schedule.rebuild');

      let modalInstance = this.$modal.open({
        animation: scope.animationsEnabled,
        templateUrl: 'cellAddRecordModal.html',
        windowClass: 'schedule-cell-add-record-modal',
        controller: ['$scope', '$modalInstance', ($scope, $modalInstance) => {

          setTimeout(() => {
            $modalInstance.dismiss('cancel');
          }, 3000);
        }],
        size: 'sm'
      });
    };

    scope.delRecord = () => {
      // console.log('delRecord');

      let modalInstance = this.$modal.open({
        animation: scope.animationsEnabled,
        templateUrl: 'cellDelRecordModal.html',
        windowClass: 'schedule-cell-del-record-modal',
        controller: ['$scope', '$modalInstance', 'data', ($scope, $modalInstance, data) => {
          $scope.data = data;

          $scope.disabled = (new Date(data.cell.startTime)).getTime() <= (new Date()).getTime();

          $scope.delRecord = () => {
            $modalInstance.close(true);
          };

          $scope.cancel = () => {
            $modalInstance.dismiss('cancel');
          };
        }],
        size: 'sm',
        resolve: {
          data: () => {
            return {
              cell: scope.currentCell,
              cellSelectedPatient: scope.cellSelectedPatient,
              specialist: scope.currentSpecialist
            };
          }
        }
      });

      modalInstance.result.then((state) => {
        if (state) {
          let ind = scope.currentSpecialist.patients.indexOf(scope.currentSpecialist.patients.find((p) => p.patient.id === scope.cellSelectedPatient.patient.id && p.date.date === scope.cellSelectedPatient.date.date && p.date.time.h === scope.cellSelectedPatient.date.time.h && p.date.time.m === scope.cellSelectedPatient.date.time.m));
          scope.currentSpecialist.patients.splice(ind, 1);

          scope.$root.$broadcast('schedule.rebuild');
        }
      });

    };

    scope.toggleSpecialistExp = ($event, specialist) => {
      specialist['exp'] = false;
      specialist['toggle'] = true;
      setTimeout(() => {
        specialist['toggle'] = false;
      }, 350);
    };

  };

  // @ngInject
  constructor(private dataService: DataService,
              private medicalInstitutionsService: MedicalInstitutionsService,
              private $filter: ng.IFilterService,
              private $modal) {
    const TYPE_WORK_OFF = 0;
    const TYPE_WORK = 1;
    const TYPE_WORK_STUDY = 2;
    const TYPE_WORK_DOCS = 3;
    const TYPE_WORK_HOME = 4;
    const TYPE_WORK_VACATION = 5;
    const TYPE_WORK_SICK_LEAVE = 6;

    // this.WORK_TYPES[TYPE_WORK_OFF] = "Врач не работает";
    this.WORK_TYPES[TYPE_WORK_OFF] = "Нет записи";
    this.WORK_TYPES[TYPE_WORK] = "Запись на прием";
    this.WORK_TYPES[TYPE_WORK_STUDY] = "Обучение";
    this.WORK_TYPES[TYPE_WORK_DOCS] = "Работа с документами";
    this.WORK_TYPES[TYPE_WORK_HOME] = "Прием на дому";
    this.WORK_TYPES[TYPE_WORK_VACATION] = "Отпуск";
    this.WORK_TYPES[TYPE_WORK_SICK_LEAVE] = "Больничный";

    medicalInstitutionsService.getAll();
    this.medicalInstitutions = medicalInstitutionsService.model;
  }

  private getMessage = (specialists) => {
    if (this.dataService.filters['specialists']['length'] === 0) {
      return this.dataService.getMessage('С 008');
    }

    if (specialists['length'] === 0) {
      return this.dataService.getMessage('С 007');
    }

    return undefined;
  };

  private prepareByPeriod = (collection: any[], period: string, date: Date) => {
    let toD = {}, cd = new Date(date);
    collection.forEach((obj) => {
      obj['schedule'].forEach((s) => {
        s['place'] = s['place'] === Object(s['place']) ? s['place'] : this.medicalInstitutions['find']((mi) => mi.id === s['place']);

        if (s.days.indexOf(cd.getDay()) !== -1) {
          toD['1'] = toD['1'] || [];
          toD['1'].push(this.getCells({
            name: obj.name,
            specialty: obj.specialty,
            date: cd,
            schedule: s,
            patients: obj.patients
          }));
        }

        if (period === '2d') {
          let date2 = new Date(date);
          date2.setDate(date2.getDate() + 1);

          if (s.days.indexOf(date2.getDay()) !== -1) {
            toD['2'] = toD['2'] || [];
            toD['2'].push(this.getCells({
              name: obj.name,
              specialty: obj.specialty,
              date: date2,
              schedule: s,
              patients: obj.patients
            }));
          }
        }

        if (period === '1w') {
          let d = {};
          for (let i = 1; i < 7; i++) {
            d[i + 1 + ''] = new Date(date);
            d[i + 1 + ''].setDate(d[i + 1 + ''].getDate() + i);

            if (s.days.indexOf(d[i + 1].getDay()) !== -1) {
              toD[i + 1 + ''] = toD[i + 1 + ''] || [];
              toD[i + 1 + ''].push(this.getCells({
                name: obj.name,
                specialty: obj.specialty,
                date: d[i + 1 + ''],
                schedule: s,
                patients: obj.patients
              }));
            }
          }
        }
      });
    });

    let result = Object.keys(toD).map((k) => toD[k]);
    return result.length > 0 ? result.reduce((a, b) => a.concat(b)) : result;
  };

  private getCells = (specialist) => {
    let cells = [];

    // start
    if (specialist.schedule.startTime.h + specialist.schedule.startTime.m > specialist.schedule.place.startTime.h + specialist.schedule.place.startTime.m) {
      cells.push({type: 0, label: "Врач не принимает"});
    }

    // if (parseInt(specialist.schedule.startTime.m, 10) % specialist.schedule.step !== 0) {
    //   cells.push({type: 0, label: "Нет записи"});
    // }

    let sd = new Date(specialist.date);
    sd.setHours(specialist.schedule.startTime.h);
    sd.setMinutes(specialist.schedule.startTime.m);
    sd.setSeconds(0);
    sd.setMilliseconds(0);
    let ed = new Date(specialist.date);
    ed.setHours(specialist.schedule.endTime.h);
    ed.setMinutes(specialist.schedule.endTime.m);
    ed.setSeconds(0);
    ed.setMilliseconds(0);

    let sdMic = sd.getTime(),
      edMic = ed.getTime(),
      stepMic = specialist.schedule.step * 60 * 1000,
      rem = edMic % stepMic;
    edMic = rem ? edMic + rem : edMic;

    for (let i = sdMic; i < edMic; i = i + stepMic) {
      let d = new Date(i),
        d2 = new Date(i + stepMic), cellType;
      let time = this.$filter('date')(d, "HHmm");
      let patientRecords = [];

      specialist.schedule.intervals.forEach((int) => {
        if (int.days.indexOf(d.getDay()) !== -1
          && int.startTime.h + int.startTime.m <= time && int.endTime.h + int.endTime.m > time) {
          cellType = int.type;
        }
      });

      if (specialist.patients['length'] > 0) {
        let dDate = this.$filter('date')(d, "dd.MM.yyyy"),
          d2Time = this.$filter('date')(d2, "HHmm");
        patientRecords = specialist.patients.filter((p) => p.date.date === dDate && p.date.time.h + p.date.time.m >= time && p.date.time.h + p.date.time.m < d2Time);
      }

      let cell = {
        startTime: d,
        endTime: d2,
        type: cellType || 0,
        label: cellType === 1 ? d : this.WORK_TYPES[cellType || 0]
      };

      if (patientRecords['length'] > 0) {
        cell['patientRecords'] = patientRecords;
        cell['title'] = patientRecords.map((pR) => pR.patient.shortName).join(' | ');
      }

      cells.push(cell);
    }

    // end
    if (specialist.schedule.endTime.h + specialist.schedule.endTime.m < specialist.schedule.place.endTime.h + specialist.schedule.place.endTime.m) {
      cells.push({type: 0, label: "Врач не принимает"});
    }

    specialist['cells'] = cells;

    return specialist;
  };

}

@directive()
export class SpecialistDirective implements ng.IDirective {

  public restrict: string = "A";

  public link: ng.IDirectiveLinkFn = (scope: ng.IScope, element: ng.IAugmentedJQuery, attrs: ng.IAttributes): void => {

    scope.$root.$on('schedule.scroll', (event, data) => {

      let h = Math.max.apply(null, element.parent().find('.specialist-time')['map']((i, el) => {
        // console.log('El-h ', angular.element(el).height(), 'i ', i);
        return angular.element(el)['height']()
      }).get());

      // console.log('Scroll ', data.scroll);
      // console.log('Element-height ', h);
      if (!scope.specialist['toggle']) {
        if (element.find('.specialist-time')['height']() === h) {
          scope.specialist['exp'] = data.scroll > h / 2;
        } else if (data.scroll === 0) {
          scope.specialist['exp'] = false;
        }
      }
    });

  };

  // @ngInject
  constructor() {
  }
}
