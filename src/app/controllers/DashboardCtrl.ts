import { IPatient, PatientsService } from "../services/PatientsService";
import { Specialist, SpecialistsService } from "../services/SpecialistsService";
import { MedicalInstitution, MedicalInstitutionsService } from "../services/MedicalInstitutionsService";
import { DataService } from "../services/DataService";

export class DashboardCtrl implements ng.IController {

  public title: string = "Dashboard";
  public medicalInstitutions: MedicalInstitution[];

  // @ngInject
  constructor(private dataService: DataService,
              private medicalInstitutionsService: MedicalInstitutionsService,
              public $scope: ng.IScope) {

    this.medicalInstitutions = medicalInstitutionsService.model;
    medicalInstitutionsService.getAll();

    $scope.viewPeriodModel = '1d';

    $scope.medicalInstitutions = this.medicalInstitutions;
  }

  $onInit = () => {

    this.$scope.$watch('viewPeriodModel', (nV) => {
      this.dataService.setFilterProp('viewPeriod', nV);
      this.$scope.$root.$broadcast('schedule.rebuild');
    });

  };

}
