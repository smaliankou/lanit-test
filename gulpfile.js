/*var gulp = require("gulp");
 var ts = require("gulp-typescript");
 var tsProject = ts.createProject("tsconfig.json");
 var browserify = require('gulp-browserify');

 // Basic usage
 gulp.task('scripts', ['typescript'], function () {
 // Single entry point to browserify
 gulp.src('src/js/app.js')
 .pipe(browserify({
 insertGlobals: true,
 debug: !gulp.env.production
 }))
 .pipe(gulp.dest('./build/js'))
 });
 //
 // gulp.task("typescript", function () {
 //     return tsProject.src()
 //         .pipe(ts(tsProject))
 //         .pipe(gulp.dest("src/js"));
 // });

 /!** Transpile TypeScript files **!/
 gulp.task('typescript', function () {
 var tsResult = tsProject.src()
 .pipe(tsProject());
 return tsResult.js.pipe(gulp.dest('./src/js'));
 });

 gulp.task("watch", function () {
 gulp.watch(['src/!**!/!*.ts'], ['default']);
 });

 gulp.task("default", ['scripts']);*/


var gulp = require('gulp'),
    webserver = require('gulp-webserver'),
    del = require('del'),
    ts = require("gulp-typescript"),
    tsProject = ts.createProject("tsconfig.json"),
    jshint = require('gulp-jshint'),
    sourcemaps = require('gulp-sourcemaps'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    uglify = require('gulp-uglify'),
    gutil = require('gulp-util'),
    sass = require("gulp-sass"),
    autoprefixer = require('gulp-autoprefixer'),
    ngAnnotate = require('browserify-ngannotate');

var CacheBuster = require('gulp-cachebust');
var cachebust = new CacheBuster();

gulp.task('compile:typescript', function () {
    var tsResult = tsProject.src()
        .pipe(tsProject());
    return tsResult.js.pipe(gulp.dest('./dist/js'));
});

/////////////////////////////////////////////////////////////////////////////////////
//
// cleans the build output
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('clean', function (cb) {
    del([
        'dist',
        'build'
    ], cb);
});

/////////////////////////////////////////////////////////////////////////////////////
//
// runs bower to install frontend dependencies
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('bower', function () {
    var install = require("gulp-install");

    return gulp.src(['./bower.json'])
        .pipe(install());
});

/////////////////////////////////////////////////////////////////////////////////////
//
// runs jshint
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('jshint', function () {
    gulp.src('/dist/js/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

/////////////////////////////////////////////////////////////////////////////////////
//
// Compile sass
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('compile:sass', ['clean'], function () {
    return gulp.src('./src/styles/app.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer({browsers: ['last 2 version']}))
        .pipe(cachebust.resources())
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('./build'));
});


/////////////////////////////////////////////////////////////////////////////////////
//
// Build a minified Javascript bundle - the order of the js files is determined
// by browserify
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('build-js', ['clean', 'compile:typescript'], function () {
    var b = browserify({
        entries: ['./dist/js/app.js'/*, './dist/js/app.module.js', './dist/js/index.js', './dist/js/router.js'*/],
        debug: true,
        paths: ['./dist/js/controllers', './dist/js/services', './dist/js/directives', './dist/js/decorators', './dist/js/filters'],
        transform: [ngAnnotate]
    });

    return b.bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(cachebust.resources())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(uglify())
        .on('error', gutil.log)
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./build/'));
});

/////////////////////////////////////////////////////////////////////////////////////
//
// full build (except sprites), applies cache busting to the main page css and js bundles
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('build', ['clean', 'bower', 'compile:typescript', 'compile:sass', 'jshint', 'build-js'], function () {
    return gulp.src(['src/index.html', 'src/patients.json', 'src/specialists.json', 'src/app/**/*.html'])
        .pipe(cachebust.references())
        .pipe(gulp.dest('build'));
});

/////////////////////////////////////////////////////////////////////////////////////
//
// launches a web server that serves files in the current directory
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('webserver', ['build'], function () {
    gulp.src('.')
        .pipe(webserver({
            livereload: false,
            directoryListing: true,
            open: "http://localhost:8000/build/index.html"
        }));
});

/////////////////////////////////////////////////////////////////////////////////////
//
// launch a build upon modification and publish it to a running server
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('dev', ['webserver']);

/////////////////////////////////////////////////////////////////////////////////////
//
// installs and builds everything
//
/////////////////////////////////////////////////////////////////////////////////////

gulp.task('default', ['build']);